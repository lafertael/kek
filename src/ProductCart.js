var Product = require('./Product');

class ProductCart extends Product
{
    constructor(dataProduct, callback)
    {
        super(dataProduct, callback);
    }

    render() 
    {
        this.container.className = 'cart-product align-items-center row mt-2';

        this.button.innerHTML = 'remove cart';

        this.name.className += ' col-sm-3';
        this.price.className += ' col-sm-3';

        this.quantity = document.createElement('p');

        this.quantity.className = 'col-sm-2'

        this.quantity.innerHTML = this.data.quantity;

        this.price.innerHTML    = this.data.sum;

        this.container.appendChild(this.name);
        this.container.appendChild(this.quantity);
        this.container.appendChild(this.price);
        this.container.appendChild(this.button);

        return this.container;
    }
   
}

module.exports = ProductCart;