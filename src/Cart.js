var ProductCart = require('./ProductCart');

class Cart 
{
    constructor(products)
    {
        this.productsCart   = [];
        this.products       = products;

        this.add            = this.add.bind(this);
        this.remove         = this.remove.bind(this);
        this.show           = this.show.bind(this);

        this.container      = document.getElementById('cart');
        this.buttonBlock    = document.getElementById('show_cart');

        this.buttonBlock.onclick = this.show;

        this.fullSum        = 0;
        this.fullQuantity   = 0;
    }
    
    show()
    {
        this.container.classList.toggle("d-none");
    }

    add(e)
    {
        var id = + e.target.dataset.productId;

        var Product = this.isProductCart(id);

        if(Product){
            Product.quantity ++;
            Product.sum += Product.price;
        }
        else{
            Product = this.getProduct(id);
            Product.sum = Product.price;
            Product.quantity = 1;
            this.productsCart.push(this.getProduct(id))
        }
        
        this.fullQuantity ++;
        this.fullSum += Product.price;

        this.render();
    }

    remove(e)
    { 
        var id = + e.target.dataset.productId;

        var Product = this.isProductCart(id);

        if(Product && Product.quantity > 1)
        {
            Product.quantity --;
            Product.sum -= Product.price;
        }
        else{
            this.removeProduct(id);
        }

        this.fullQuantity --;
        this.fullSum -= Product.price;

        this.render();
    }

    isProductCart(id)
    {
        for(var j in this.productsCart)
        {
            if(+this.productsCart[j]['id'] === id)
            {
                return this.productsCart[j];
            }
        }
        return false;
    }

    getProduct(id)
    {
        for(var j in this.products)
        {
            if(+this.products[j]['id'] === id)
            {
                return this.products[j]
            }
        }
    }

    removeProduct(id)
    {
        this.productsCart = this.productsCart.filter((product) =>
        {
            if(+product['id'] === id)
            {
                return false;
            }
            return true;
        });
    }

    render()
    {
        this.container.innerHTML = "";

        for(var j in this.productsCart)
        {
            var product = new ProductCart(this.productsCart[j], this.remove);

            this.container.appendChild(product.render());
        }

        this.buttonBlock.innerHTML = 'full sum :' + this.fullSum + ', full quantity:' + this.fullQuantity;
    }
}

module.exports = Cart;